const fs = require('fs');

const input = fs.readFileSync('./input.txt', 'utf8').split(',').map(el => parseInt(el));


function findOutput(output, data) {
  let value;

  for (let noun = 0; noun < 100; noun++) {
    for (let verb = 0; verb < 100; verb++) {
      const ints = updateInput(data, noun, verb);
      result = intCode(ints);
      if (result === output) value = [noun, verb];
    }
  }

  return 100 * value[0] + value[1];
}

function intCode(arr) {

  let con = true;
  let indices = [0, 4]
  while (con) {
    const section = arr.slice(...indices)
    con = compute(section, arr)
    indices = [indices[0] + 4, indices[1] + 4]
  }

  return arr[0];
}

function compute(section, arr) {
  const [action, pos1, pos2, store] = section;
  const [num1, num2] = [arr[pos1], arr[pos2]];

  if (action === 1) {
    arr[store] = num1 + num2
  }

  if (action == 2) {
    arr[store] = num1 * num2
  }

  if (action === 99) {
    return false;
  }

  return true;
}

function updateInput(arr, in1, in2) {
  data = Array.from(arr);
  data[1] = in1
  data[2] = in2

  return data;
}

// console.log(intCode(updateInput(input, 71, 95)));
console.log(findOutput(19690720, input))

const fs = require('fs');

const input = fs.readFileSync('./input.txt', 'utf8');

const range = input.split('-').map(el => parseInt(el))

function countPasswords(range) {
  const [start, end] = range;
  let count = 0;
  
  for (let curr = start; curr < end; curr++) {
    if (isValid(curr)) {
      count = count + 1;
    }
  }

  return count;
}

function isValid(int) {
  const chars = int.toString().split("");

  return isAscending(chars) && hasDouble(chars);
}

function isAscending(chars) {
  for (let i = 0; i < chars.length - 1; i++) {
    const element = chars[i].charCodeAt();
    const next = chars[i + 1].charCodeAt();

    if (next < element) {
      return false;
    }
  }

  return true;
}

function hasDouble(chars) {
  let counts = {};
  let valid = false;

  chars.forEach(char => {
    if (counts[char]) {
      counts[char] = counts[char] + 1
    } else {
      counts[char] = 1;
    }
  });

  Object.values(counts).forEach(val => { 
    if (val == 2) valid = true; 
  });

  return valid;
}

// console.log(isValid(112233))
// console.log(isValid(123444))
// console.log(isValid(111122))

console.log(countPasswords(range));
// SHAME

class OrbitNode {
  constructor(value, parent=null) {
    this.value = value;
    this.parent = parent;
    this.children = [];
  }

  assignParent(parent) {
    this.parent = parent;


    if (!parent.children.includes(this)) {
      parent.children.push(this);
    }

    return parent
  }

  directOrbits() {
    return this.children.length
  }

  indirectOrbits() {
    if (this.children.length === 0) return 0;

    let total = 0;

    this.children.forEach(child => {
      total = total + child.totalOrbits() + 1;
    });

    return total;
  }

  totalOrbits() {
    const direct = this.directOrbits();
    const indirect = this.indirectOrbits();
    return direct + indirect;
  }
}

module.exports = OrbitNode;
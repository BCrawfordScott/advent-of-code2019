const { readFileSync } = require('fs');
const OrbitNode = require('./orbitNode.js');

const orbits = readFileSync('./input.txt', 'utf8').trim().split('\n');

const PLANETS = {};

function YOUtoSAN() {
  buildOrbits();
  const start = PLANETS['YOU'];
  const finish = PLANETS['SAN'];

  const startDistances = {};

  let traveling = start;
  let moves = 0;

  while (traveling !== 'COM') {
    moves = moves + 1;
    traveling = PLANETS[traveling];
    startDistances[traveling] = moves;
  }

  traveling = finish;
  moves = 0;
  while (traveling !== 'COM') {
    moves = moves + 1;
    traveling = PLANETS[traveling];
    if (startDistances[traveling]) {
      return moves + startDistances[traveling];
    }
  }
}

function orbitTotals() {
  console.log(PLANETS)

  let total = 0;

  Object.keys(PLANETS).forEach(key => {
    console.log(key);
    let curr = key;
    while (curr !== 'COM') {
      curr = PLANETS[curr];
      total = total + 1;
    }
  });

  return total;
}

function buildOrbits() {
  orbits.forEach(orbit => {
    const { parent, child } = parseOrbit(orbit);

    PLANETS[child] = parent
  });
}

function parseOrbit(orbit) {
  const [parent, child] = orbit.split(')');

  return {
    parent,
    child,
  };
}

function execute() {
  
  buildOrbits();
  return orbitTotals();
}

// console.log(execute());

console.log(YOUtoSAN())
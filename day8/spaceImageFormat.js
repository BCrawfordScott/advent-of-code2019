const { readFileSync } = require('fs');

const program = readFileSync('./input.txt', 'utf8').split("").map(el => parseInt(el));
// console.log(validate(program, 25, 6));

generateImage(program, 25, 6);

function generateImage(input, width, height) {
  const layers = getLayers(input, (width * height));
  const formatted = formatLayers(layers, width);
  const image = compileImage(formatted);

  printImage(image);
}

function printImage(image) {

  const formattedImage = image.map(line => line.join('').replace(/0/g, ' '));
  formattedImage.forEach(line => console.log(line));
}

function compileImage(layers) {

  const layerKeys = Object.keys(layers);

  const topLayer = layers['1'];
  console.log(topLayer);
  const image = topLayer;

  layerKeys.forEach(key => {
    if(key === '1') return;
    const nextLayer = layers[key];

    for (let i = 0; i < topLayer.length; i++) {
      const topLine = topLayer[i];
      const nextLine = nextLayer[i];

      applyLine(topLine, nextLine);
    }
  });

  return image;
}

function applyLine(topLine, nextLine) {
  for (let i = 0; i < nextLine.length; i++) {
    const pixel = nextLine[i];

    if (topLine[i] === 2) topLine[i] = pixel;
  }

  return topLine;
}

function formatLayers(layers, width) {
  const formattedLayers = {};

  Object.keys(layers).forEach(key => {
    const formatted = [];
    let layer = layers[key];

    for (let i = 0; i < layer.length / width; i++) {

      const start = i * width;
      const end = (i + 1) * width;

      formatted.push(layer.slice(start, end));
    }

    formattedLayers[key] = formatted;
  });

  return formattedLayers;
}

function validate(input, width, height) {
  const layers = getLayers(input, (width * height));
  const layer = densestLayer(layers);

  return calcDigits(layer);
}

function calcDigits(layer) {
  const counts = {};

  layer.forEach(el => {
    const count = counts[el];
    if (count) {
      counts[el] = count + 1;
    } else {
      counts[el] = 1;
    }
  });

  return counts['1'] * counts['2'];
}

function densestLayer(layers) {
  let densest;

  Object.keys(layers).forEach(key => {
    let layer = layers[key];
    let zeroCount = 0;

    layer.forEach(el => {
      if (el === 0) zeroCount = zeroCount + 1;
    });

    if (!densest || zeroCount < densest.zeroCount) densest = { zeroCount, layer };
  });

  return densest.layer;
}

function getLayers(input, dimension) {
  const layers = {};

  for (let i = 0; i < input.length / dimension; i++) {

    const start = i * dimension;
    const end = (i + 1) * dimension;

    layers[i + 1] = input.slice(start, end); 
  }

  return layers;
}
const { readFileSync } = require('fs');

class ArrayMap extends Map {
  set(key, value) {
    if (!this[key]) {
      this[key] = [];
    } 

    this[key].push(value);
  }
}

class Asteroid {

  constructor(position) {
    this.pos = position;
    this.neighbors = new ArrayMap();
  }

  visibleNeighborCount() {
    return Object.keys(this.neighbors).length;
  }

  distanceFrom(asteroid) {
    Asteroid.calcDistance(this, asteroid);
  }

  findNeighbors(asteroids) {
    asteroids.forEach(asteroid => {
      if (asteroid === this) return;
      const direction = Asteroid.calcDirection(this, asteroid).join(',');
      const seen = this.neighbors[direction];

      if(seen) {
        this.neighbors.set(direction, asteroid);
        this.neighbors[direction].sort((ast1, ast2) => {
          let [x, y] = [ast1.distanceFrom(this), ast2.distanceFrom(this)];
          if (y > x) return 1;
          if (x > y) return -1;
          return 0;
        })
      } else {
        this.neighbors.set(direction, asteroid);
      } 
    });
  }
}

Asteroid.sortPositions = function(asteroids) {
  return Object.keys(asteroids).sort((pos1, pos2) => {
    const [x1, y1] = pos1.split(',').map(el => parseInt(el));
    const [x2, y2] = pos2.split(',').map(el => parseInt(el));
    const ang1 = Math.atan2(x1, y1)
    const ang2 = Math.atan2(x2, y2)
    
    if (ang1 < ang2) return -1
    if (ang1 > ang2) return 1
    return 0;
  });
}

Asteroid.calcDistance = function(ast1, ast2) {
  const [x, y] = ast1.pos;
  const [n, m] = ast2.pos;

  return Math.abs(n - x) + Math.abs(m - y);
};

Asteroid.calcDirection = function(ast1, ast2) {
  const [x, y] = ast1.pos;
  const [n, m] = ast2.pos;

  return gCPF(n - x, m - y);
};

Asteroid.parseStringMap = function (stringMap) {
  const asteroids = [];
  stringMap.split('\n').forEach((row, y) => {
    row.split('').forEach((el, x) => {
      if (el === '.') return;
      asteroids.push(new Asteroid([x, y]));
    });
  });

  return asteroids;
};

function gCPF(n, m) {
  let [x, y] = [n, m];

  if (Math.abs(x) === Math.abs(y)) return [x / Math.abs(x), y / Math.abs(y)];
  if (x === 0) return [x, y / Math.abs(y)];
  if (y === 0) return [x / Math.abs(x), y];
  for (let i = 2; i <= Math.min(Math.abs(x), Math.abs(y)); i++) {
    while (x % i === 0 && y % i === 0) {
      x = x / i;
      y = y / i;
    }
  }

  return [x,y];
}

function isPrime(x) {
  for (let i = 2; i < x / 2; i++) {
    if (x % i === 0) return false;
  }

  return true;
}

const input = readFileSync('input.txt', 'utf8');

function findBestAsteroid(input) {
  const asteroidList = Asteroid.parseStringMap(input);
  let best;
  asteroidList.forEach(asteroid => {
    asteroid.findNeighbors(asteroidList);
    if(!best || best.visibleNeighborCount() < asteroid.visibleNeighborCount()) best = asteroid;
  });
  // console.log(asteroidList);
  return best;
}

function findNthDestroyed(n, input) {
  const asteroid = findBestAsteroid(input);
  const asteroidList = asteroid.neighbors;

  const clockWise = Asteroid.sortPositions(asteroidList);

  let index;
  clockWise.forEach((el, idx) => {
    if (index) return;
    const pos = el.split(',').map(el => parseInt(el));
    if (pos[1] >= 0) index = idx; 
  })
  
  let destroyed = 0;
  let last;
  while(!last) {
    const key = clockWise[index % clockWise.length];
    const currAsteroid = asteroidList[key].pop();
    if (currAsteroid) {
      destroyed = destroyed + 1;
      if (destroyed === n) last = currAsteroid;
    }

    index = index + 1;
  }

  return last;
}



const testInput = ".#..##.###...#######\n##.############..##.\n.#.######.########.#\n.###.#######.####.#.\n#####.##.#.##.###.##\n..#####..#.#########\n####################\n#.####....###.#.#.##\n##.#################\n#####.##.###..####..\n..######..##.#######\n####.##.####...##..#\n.#####..#.######.###\n##...#.##########...\n#.##########.#######\n.####.#.###.###.#.##\n....##.##.###..#####\n.#.#.###########.###\n#.#.#.#####.####.###\n###.##.####.##.#..##"




console.log(findNthDestroyed(200, testInput));

// const testInput = ".#..#\n.....\n#####\n....#\n...##";
// const testInput = ".#..#..###\n####.###.#\n....###.#.\n..###.##.#\n##.##.#.#.\n....###..#\n..#.#..#.#\n#..#.#.###\n.##...##.#\n.....#.#.."
// const testInput = "......#.#.\n#..#.#....\n..#######.\n.#.#.###..\n.#..#.....\n..#....#.#\n#..#....#.\n.##.#..###\n##...#..#.\n.#....####";
// const best = findBestAsteroid(input);
// console.log(best, best.visibleNeighborCount());

// class Asteroid {

//   parseStringMap(stringMap) {
//     return stringMap.split('\n').map((row, y) => {
//       return row.split('').map((el, x) => {
//         if (el === '.') return null;
//         const asteroid = new Asteroid([x, y], this);
//         this.asteroids.set([x, y], asteroid)
//         return asteroid;
//       })
//     })
//   }

//   access(pos) {
//     const [x, y] = pos;
//     return this.astroMap[y][x];
//   }

//   print() {
//     console.log(this.astroMap);
//   }

//   asteroidWithBestVisibility() {
//     let greatest = { coor: null, count: 0 };
//     Array.from(this.asteroids.values().forEach(asteroid => {
//       const count = asteroid.visibleNeighborCount();
//       if ( count > greatest.count) {
//         greatest = { coor: asteroid.pos, count: count };
//       }
//     });

//     return greatest;
//   }
// }



// function between(x, min, max) {
//   return (x >= min && x < max);
// }



// function returnInput() {
//   return [
//     ".............#..#.#......##........#..#",
//     ".#...##....#........##.#......#......#.",
//     "..#.#.#...#...#...##.#...#.............",
//     ".....##.................#.....##..#.#.#",
//     "......##...#.##......#..#.......#......",
//     "......#.....#....#.#..#..##....#.......",
//     "...................##.#..#.....#.....#.",
//     "#.....#.##.....#...##....#####....#.#..",
//     "..#.#..........#..##.......#.#...#....#",
//     "...#.#..#...#......#..........###.#....",
//     "##..##...#.#.......##....#.#..#...##...",
//     "..........#.#....#.#.#......#.....#....",
//     "....#.........#..#..##..#.##........#..",
//     "........#......###..............#.#....",
//     "...##.#...#.#.#......#........#........",
//     "......##.#.....#.#.....#..#.....#.#....",
//     "..#....#.###..#...##.#..##............#",
//     "...##..#...#.##.#.#....#.#.....#...#..#",
//     "......#............#.##..#..#....##....",
//     ".#.#.......#..#...###...........#.#.##.",
//     "........##........#.#...#.#......##....",
//     ".#.#........#......#..........#....#...",
//     "...............#...#........##..#.#....",
//     ".#......#....#.......#..#......#.......",
//     ".....#...#.#...#...#..###......#.##....",
//     ".#...#..##................##.#.........",
//     "..###...#.......#.##.#....#....#....#.#",
//     "...#..#.......###.............##.#.....",
//     "#..##....###.......##........#..#...#.#",
//     ".#......#...#...#.##......#..#.........",
//     "#...#.....#......#..##.............#...",
//     "...###.........###.###.#.....###.#.#...",
//     "#......#......#.#..#....#..#.....##.#..",
//     ".##....#.....#...#.##..#.#..##.......#.",
//     "..#........#.......##.##....#......#...",
//     "##............#....#.#.....#...........",
//     "........###.............##...#........#",
//     "#.........#.....#..##.#.#.#..#....#....",
//     "..............##.#.#.#...........#.....",
//   ]
// }
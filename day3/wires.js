const fs = require('fs');

const path1 = fs.readFileSync('input.txt', 'utf8').split(',')
const path2 = fs.readFileSync('input2.txt', 'utf8').split(',')

const DELTAS = [
  [1, 0],
  [0, 1],
  [-1, 0],
  [0, -1],
];

const path1Coords = {};
const path2Coords = {};
const intersectingCoords = [];

function genLine1(first = true) {
  
  genLine(path1, first)
}

function genLine2(first = false) {

  genLine(path2, first)
}

function genLine(path, path1 = true) {
  let currPos = [1, 1]
  let steps = 1;
  const handler = path1 ? populateP1Coords : populateP2Coords;

  path.forEach(instr => {
    const [dir, dist] = parseInstruction(instr);

    const [dx, dy] = getDelta(dir);

    for (let i = 0; i < dist; i++) {
      const [cx, cy] = currPos;
      currPos = [cx + dx, cy + dy];

      handler(currPos, steps);
      steps = steps + 1
    }
  })
}

function parseInstruction(instr) {
  return [instr.slice(0, 1), instr.slice(1)];
}

function getDelta (dir) {
  switch (dir) {
    case "U":
      return DELTAS[0];
    case "R":
      return DELTAS[1];
    case "D":
      return DELTAS[2];
    case "L":
      return DELTAS[3];
    default:
      throw "somethin' ain't right";
  }
}

function populateP1Coords(coord, steps) {
  const key = coord.join(',')
  if (!path1Coords[key]) {
    path1Coords[key] = steps;
  }
}
function populateP2Coords(coord, steps) {
  const key = coord.join(',')
  if (!path2Coords[key]) {
    path2Coords[key] = steps;
  }
}

function checkIntersection(coord, steps) {
  if (visitedCoords.has(coord.join(','))) {
    intersectingCoords.push(coord);
  }
}

function findShortestDist() {
  let shortest;

  intersectingCoords.forEach(coord => {
    const dist = manhattanDist(coord, [1,1]);
    if (!shortest || shortest > dist) {
      shortest = dist;
    }
  })

  return shortest;
}

function manhattanDist(coord, start = [1,1]) {
  const [dx, dy] = coord;
  const [sx, sy] = start;

  return Math.abs(sx - dx) + Math.abs(sy - dy)
}

function findShortestSteps() {
  let shortest;
  Object.keys(path2Coords).forEach(coord => {
    if (path1Coords[coord]) {
      const p1Steps = path1Coords[coord]
      const p2Steps = path2Coords[coord]

      const total = p1Steps + p2Steps;

      if (!shortest || shortest > total) {
        shortest = total;
      }
    }
  })

  return shortest;
}

genLine1()
genLine2()

console.log(findShortestSteps());
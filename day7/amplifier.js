const { readFileSync } = require('fs');
const AmplifyNode = require('./amplifyNode.js');

const input = readFileSync('./input.txt', 'utf8').split(',').map(el => parseInt(el));

// console.log(findBiggestAmp(input))

console.log(feedbackAmp(input));


function feedbackAmp(program) {
  const SEQUENCES = permutations([5,6,7,8,9]);
  let largest = 0;
  const outputs = {};

  SEQUENCES.forEach(sequence => {
    const loop = AmplifyNode.buildList(sequence, program);

    const curr = AmplifyNode.executeLoop(loop);

    if (curr > largest) largest = curr;
    outputs[curr] = sequence;
  });
  
  console.log(outputs[largest]);
  return largest;
}







function findBiggestAmp(program) {
  const outputs = {};
  let largest = 0;
  const SEQUENCES = permutations([0, 1, 2, 3, 4]);

  SEQUENCES.forEach(sequence => {
    const curr = computeAmplifier(sequence, program);
    // console.log(curr);
    if (curr > largest) largest = curr;
    outputs[curr] = sequence;
  })


  console.log(outputs[largest]);
  return largest;
}

// function testing(sequence, testInput) {
//   return computeAmplifier(sequence, testInput);
// }







// function computeAmplifier(sequence, program=input) {
//   const cleanProgram = program.slice();

//   let output = 0;
//   sequence.forEach(setting => {
//     output = runIntCode(setting, output, cleanProgram);
//   });
//   // if (sequence.length === 0) return 0;

//   return output;
//   // const setting = sequence[sequence.length - 1];
//   // return runIntCode(setting, cleanProgram, sequence.slice(0, sequence.length - 1));
// }

// // iterate through the sequence values
// // for each value, 
//   // run the intCode with the setting 
//   // if there is a value from the last iteration, feed that in as the second input, if not, feed in 0
//   // capture the return value of the intCode to be used as the next second input
// // output the last value provided.








// ////////////////////////////////////////////////////////////////


// function runIntCode(setting, output, program) {


//   let pointer = 0;
//   let con = true;
//   let signal;
//   let settings = { supply: true, setting, output };
//   let data;

//   while (con) {
//     let opCode = program[pointer];
//     let commands = parseOpCode(opCode);

//     data = compute(commands, pointer, program, settings);

//     con = data.con;
//     settings = data.settings;
//     pointer = data.pointer;

//     if (data.signal) signal = data.signal;
//   }

//   return signal;
// }

// function compute(commands, pointer, program, settings) {

//   const [code, modes] = commands;
//   let x, y;



//   switch (code) {
//     case 99:
//       return {
//         con: false,
//       };
//     case 8:
//       [x, y] = getValues(modes, pointer, program);
//       if (x === y) {
//         program[program[pointer + 3]] = 1;
//       } else {
//         program[program[pointer + 3]] = 0;
//       }
//       return { 
//         con: true, 
//         pointer: pointer + 4,
//         settings,
//       };
//     case 7:
//       [x, y] = getValues(modes, pointer, program)
//       if (x < y) {
//         program[program[pointer + 3]] = 1;
//       } else {
//         program[program[pointer + 3]] = 0;
//       }

//       return {
//         con: true,
//         pointer: pointer + 4,
//         settings,
//       };
//     case 6:
//       [x, y] = getValues(modes, pointer, program)
//       if (!x) {
//         return {
//           con: true,
//           pointer: y,
//           settings,
//         };
//       }

//       return {
//         con: true,
//         pointer: pointer + 3,
//         settings,
//       };
//     case 5:
//       [x, y] = getValues(modes, pointer, program);
//       if (x) {
//         return {
//           con: true,
//           pointer: y,
//           settings,
//         };
//       }

//       return {
//         con: true,
//         pointer: pointer + 3,
//         settings,
//       };
//     case 4:
//       // console.log(program[program[pointer + 1]]);
//       // return [true, pointer + 2, program[program[pointer + 1]]];
//       return {
//         con: false,
//         pointer: pointer + 2,
//         settings,
//         signal: program[program[pointer + 1]],
//       };
//     case 3:
//       // console.log(settings.supply);
//       if (settings.supply) {
//         program[program[pointer + 1]] = settings.setting;
//       } else {
//         program[program[pointer + 1]] = settings.output;
//       }
//       // applyInput(program, program[pointer + 1]);
//       // return [true, pointer + 2];
//       return {
//         con: true,
//         pointer: pointer + 2,
//         settings: Object.assign({}, settings, { supply: false }),
//       };
//     case 2:
//       [x, y] = getValues(modes, pointer, program);
//       program[program[pointer + 3]] = x * y;
//       return {
//         con: true,
//         pointer: pointer + 4,
//         settings,
//       };
//     case 1:
//       [x, y] = getValues(modes, pointer, program);
//       program[program[pointer + 3]] = x + y;
//       return {
//         con: true,
//         pointer: pointer + 4,
//         settings,
//       };
//     default:
//       console.log(`your code was ${code} and something went wrong`);
//       return { con: false };
//   }
// }

// function applyInput(testData, idx) {
//   testData[idx] = 5;
// }

// function getValues(modes, pointer, testData) {

//   const first = testData[pointer + 1];
//   const x = modes[1] === 1 ? first : testData[first];

//   const second = testData[pointer + 2];
//   const y = modes[0] === 1 ? second : testData[second];

//   return [x, y]
// }

// function parseOpCode(opCode) {
//   const parseable = opCode.toString();
//   const { length } = parseable;

//   if (opCode === 4) return [opCode];
//   if (opCode === 3) return [opCode];

//   const command = parseInt(parseable.slice(length - 2));

//   if (command === '99') return [0, 99]

//   let modes = parseable.slice(0, length - 2).split("").map(el => parseInt(el));
//   modes = modes || [];
//   while (modes.length < 2) {
//     modes = [0].concat(modes);
//   }

//   return [command, modes];
// }

function permutations(arr) {
  
  if (arr.length <= 1) return [arr];
  const result = [];

  const first = arr.slice(0, 1);
  // const rest = arr.slice(1);

  const perms = permutations(arr.slice(1));
  perms.forEach(perm => {
    for (let i = 0; i < arr.length; i++) {
      const newPerm = perm.slice(0, i).concat(first.concat(perm.slice(i, perm.length))); 
      result.push(newPerm);
    }
  });

  return result;
}

// // console.log(permutations([1,2,3, 4]));
function getValues(modes, pointer, program) {

  const first = program[pointer + 1];
  const x = modes[1] === 1 ? first : program[first];

  const second = program[pointer + 2];
  const y = modes[0] === 1 ? second : program[second];

  return [x, y];
}

function parseOpCode(opCode) {
  const parseable = opCode.toString();
  const { length } = parseable;

  if (opCode === 4) return [opCode];
  if (opCode === 3) return [opCode];

  const command = parseInt(parseable.slice(length - 2));

  if (command === '99') return [0, 99]

  let modes = parseable.slice(0, length - 2).split("").map(el => parseInt(el));
  modes = modes || [];
  while (modes.length < 2) {
    modes = [0].concat(modes);
  }

  return [command, modes];
}

class AmplifyNode {
  constructor(name, setting, program) {
    this.name = name;
    this.setting = setting;
    this.next = null;
    this.pointer = 0;
    this.program = program.slice();
    this.input = null;
    this.output = null;
    this.halt = false;
  }

  sendOutput() {
    return this.output;
  }

  setOutput(val) {
    this.output = val;
    return this.sendOutput();
  }

  setInput(val) {
    this.input = val;
    return val;
  }

  setNext(node) {
    this.next = node;
    return node;
  }

  updatePointer(val) {
    this.pointer = val;
    return this.pointer;
  }

  opCode() {
    return this.program[this.pointer];
  }

  run() {
    let con = true;
    let commands;

    while (con) {
      commands = parseOpCode(this.opCode());
      con = this.compute(commands);
    }

    if (this.halt && this.next.halt) {
      return this.output;
    }

    this.next.setInput(this.output);
    return this.next.run();
    
  }

  compute(commands) {

    const { pointer, program } = this;
    const [code, modes] = commands;
    let x, y;

    switch (code) {
      case 99:
        this.halt = true;
        return false;
      case 8:
        [x, y] = getValues(modes, pointer, program);
        if (x === y) {
          program[program[pointer + 3]] = 1;
        } else {
          program[program[pointer + 3]] = 0;
        }

        this.updatePointer(pointer + 4);

        return true;
      case 7:
        [x, y] = getValues(modes, pointer, program);
        if (x < y) {
          program[program[pointer + 3]] = 1;
        } else {
          program[program[pointer + 3]] = 0;
        }

        this.updatePointer(pointer + 4);

        return true;
      case 6:
        [x, y] = getValues(modes, pointer, program)
        if (!x) {
          this.updatePointer(y);

          return true;
        }

        this.updatePointer(pointer + 3);

        return true;
      case 5:
        [x, y] = getValues(modes, pointer, program);
        if (x) {
          this.updatePointer(y);

          return true;
        }

        this.updatePointer(pointer + 3);

        return true;
      case 4:
        this.updatePointer(pointer + 2);
        this.setOutput(program[program[pointer + 1]]);

        return false;
      case 3:
        if (this.setting) {
          program[program[pointer + 1]] = this.setting;
          this.setting = null;
        } else {
          program[program[pointer + 1]] = this.input;
        }

        this.updatePointer(pointer + 2);

        return true;
      case 2:
        [x, y] = getValues(modes, pointer, program);

        program[program[pointer + 3]] = x * y;

        this.updatePointer(pointer + 4);
        
        return true;
      case 1:
        [x, y] = getValues(modes, pointer, program);

        program[program[pointer + 3]] = x + y;

        this.updatePointer(pointer + 4);

        return true;
      default:
        console.log(`Failed on amp ${this.name}. Your code was ${code} and something went wrong`);
        return false;
    }
  }
}

AmplifyNode.buildList = function(sequence, program) {
  const nodeList = sequence.map((setting, idx) => {
    return new AmplifyNode(idx, setting, program);
  });

  for (let i = 0; i < nodeList.length; i++) {
    nodeList[i].setNext(nodeList[i + 1]);

    if ((i + 1) === nodeList.length) nodeList[i].setNext(nodeList[0]);   
  }

  return nodeList[0];
};

AmplifyNode.executeLoop = function(loop) {
  return loop.run();
};

module.exports = AmplifyNode;
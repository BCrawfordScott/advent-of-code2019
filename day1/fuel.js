/*
Fuel required to launch a given module is based on its mass. 
Specifically, to find the fuel required for a module, 
take its mass, divide by three, round down, and subtract 2.
*/
const fs = require('fs')

const input = fs.readFileSync('./input.txt', 'utf8').split("\n").map(el => parseInt(el));
// input = [100756]

result = input.reduce((acc, el) => {
  const computed = Math.floor(el / 3) - 2;

  const totalFuel = calcFuel(computed);

  return acc + totalFuel;
}, 0)

function calcFuel(current) {
  console.log(current);
  if (current <= 0) return 0;
  const computed = Math.floor(current / 3) - 2;

  const fuelNeed = computed > 0 ? computed : 0;

  return current + calcFuel(fuelNeed);
}

console.log(result);
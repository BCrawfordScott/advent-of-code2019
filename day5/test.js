const fs = require('fs');

const input = fs.readFileSync('./input.txt', 'utf8').split(',').map(el => parseInt(el));
// const testInput = [3, 0, 4, 0, 99];

function getTestOutput(testInput) {
  const testData = testInput.slice();


  let pointer = 0;
  let con = true;

  while (con) {

    let opCode = testData[pointer];

    let commands = parseOpCode(opCode);

    [con, pointer] = compute(commands, pointer, testData);

  }

  return testData[0];
}

function parseOpCode(opCode) {
  const parseable = opCode.toString();
  const { length } = parseable;

  if (opCode === 4) return [opCode];
  if (opCode === 3) return [opCode];

  const command = parseInt(parseable.slice(length - 2));

  if (command === '99') return [0, 99]

  let modes = parseable.slice(0, length - 2).split("").map(el => parseInt(el));
  modes = modes || [];
  while (modes.length < 2) {
    modes = [0].concat(modes);
  }

  return [command, modes]
}

function compute(commands, pointer, testData) {

  const [code, modes] = commands;
  let x, y;



  switch(code) {
    case 99:
      return [false]
    case 8:
      [x, y] = getValues(modes, pointer, testData)
      if (x === y) {
        testData[testData[pointer + 3]] = 1;
      } else {
        testData[testData[pointer + 3]] = 0;
      }
      return [true, pointer + 4]
    case 7:
      [x, y] = getValues(modes, pointer, testData)
      if (x < y) {
        testData[testData[pointer + 3]] = 1;
      } else {
        testData[testData[pointer + 3]] = 0;
      }

      return [true, pointer + 4]
    case 6:
      [x, y] = getValues(modes, pointer, testData)
      if (!x) {
        return [true, y]
      }

      return [true, pointer + 3]
    case 5:
      [x, y] = getValues(modes, pointer, testData);
      if (x) {
        return [true, y]
      }

      return [true, pointer + 3]
    case 4:
      console.log(testData[testData[pointer + 1]]);
      return [true, pointer + 2];
    case 3:
      applyInput(testData, testData[pointer + 1]);
      return [true, pointer + 2];
    case 2:
      [x, y] = getValues(modes, pointer, testData);
      testData[testData[pointer + 3]] = x * y;
      return [true, pointer + 4]
    case 1:
      [x, y] = getValues(modes, pointer, testData);
      testData[testData[pointer + 3]] = x + y;
      return [true, pointer + 4]
    default:
      console.log(`your code was ${code} and something went wrong`);
      return [false];
  }
}

function applyInput(testData, idx) {
  testData[idx] = 5;
}

function getValues(modes, pointer, testData) {

  const first = testData[pointer + 1];
  const x = modes[1] === 1 ? first : testData[first];

  const second = testData[pointer + 2];
  const y = modes[0] === 1 ? second : testData[second];

  return [x,y]
}

getTestOutput(input);


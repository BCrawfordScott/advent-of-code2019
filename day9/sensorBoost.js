const { readFileSync } = require('fs');

function getProgram() {
  const program = readFileSync('./input.txt', 'utf8').split(',').map(el => parseInt(el));
  return program;
}

class IntCode {
  constructor(setting, program){
    this.setting = setting;
    this.program = program;
    this.pointer = 0;
    this.input;
    this.output;
    this.halt = false;
    this.relativeBase = 0;
    this.collectedOutputs = []
    this.next = this;
  }

  opCode(){
    return this.program[this.pointer];
  }

  access(idx) {
    const val = this.program[idx];
    if (val) return val;
    this.program[idx] = 0;
    return 0;
  }

  updatePointer(idx){
    this.pointer = idx;
  }

  setOutput(val) {
    this.output = val;
    return this.output;
  }

  parseOpCode(opCode) {
    const command = opCode % 100;
    const modes = Math.floor(opCode / 100).toString().split('').reverse().map(el => parseInt(el));

    while(modes.length < 3) {
      modes.push(0);
    }

    return { command, modes };
  }

  getAddresses(modes) {
    return modes.map((mode, idx) => {
      const raw = this.access(this.pointer + idx + 1);
      if (mode === 2) return this.relativeBase + raw;
      if (mode === 1) return this.pointer + idx + 1;
      return raw;
    });
  }

  run() {
    let con = true;
    let instructions;

    while (con) {
      instructions = this.parseOpCode(this.opCode());
      con = this.compute(instructions);
    }

    if (this.halt) {
      console.log(this.collectedOutputs);
      return this.output;
    } else {
      this.next.input = this.output;
      this.next.run();
    }
  }

  compute(instructions) {
    const { command, modes } = instructions;
    const [address1, address2, address3] = this.getAddresses(modes);

    switch(command) {
      case 99:
        this.halt = true;
        return false;
      case 9: 
        this.relativeBase = this.relativeBase + this.access(address1);
        this.updatePointer(this.pointer + 2);
        return true;
      case 8:
        if (this.access(address1) === this.access(address2)) {
          this.program[address3] = 1;
        } else {
          this.program[address3] = 0;
        }

        this.updatePointer(this.pointer + 4);
        return true;
      case 7:
        if (this.access(address1) < this.access(address2)) {
          this.program[address3] = 1;
        } else {
          this.program[address3] = 0;
        }

        this.updatePointer(this.pointer + 4);
        return true;
      case 6:
        if (!this.access(address1)) {
          this.updatePointer(this.access(address2));
          return true;
        }

        this.updatePointer(this.pointer + 3);
        return true;
      case 5:
        if (this.access(address1)) {
          this.updatePointer(this.access(address2));
          return true;
        }

        this.updatePointer(this.pointer + 3);
        return true;
      case 4:
        console.log(address1)
        this.updatePointer(this.pointer + 2);
        this.setOutput(this.access(address1));
        this.collectedOutputs.push(this.output);
        return true;
      case 3:
        if (this.setting) {
          this.program[address1] = this.setting;
          this.setting = null;
        } else {
          this.program[address1] = this.input;
        }

        this.updatePointer(this.pointer + 2);
        return true;
      case 2: 
        this.program[address3] = this.access(address1) * this.access(address2);
        this.updatePointer(this.pointer + 4);
        return true;
      case 1:
        this.program[address3] = this.access(address1) + this.access(address2);
        this.updatePointer(this.pointer + 4);
        return true;
      default:
        console.log(`Something went wrong:\nCommand: ${command}\nModes: ${modes}\nPointer: ${this.pointer}\nCollected values: ${this.collectedOutputs}`)
        this.halt = true;
        return false;
    }
  }
}

// console.log((new IntCode(1, [1102, 34915192, 34915192, 7, 4, 7, 99, 0])).run());
console.log((new IntCode(2, getProgram())).run());
// console.log((new IntCode(1, [109, 1, 9, 2, 204, -6, 99])).run());


























































// class SensorNode {
//   constructor(name, setting, program) {
//     this.name = name;
//     this.setting = setting;
//     this.next = null;
//     this.pointer = 0;
//     this.program = program.slice();
//     this.input = null;
//     this.output = null;
//     this.halt = false;
//     this.relativeBase = 0;
//     this.collectedOutputs = [];
//   }

//   setOutput(val) {
//     this.output = val;
//     return this.ouput;
//   }

//   setInput(val) {
//     this.input = val;
//     return val;
//   }

//   setNext(node) {
//     this.next = node;
//     return node;
//   }

//   updatePointer(val) {
//     this.pointer = val;
//     return this.pointer;
//   }

//   opCode() {
//     return this.program[this.pointer];
//   }

//   access(idx) {
//     const val = this.program[idx];
//     if (!val) this.program[idx] = 0;
//     return this.program[idx];
//   }

//   run() {
//     let con = true;
//     let commands;

//     while (con) {
//       console.log(this.pointer, this.program[this.pointer])
//       // console.log(this.program);
//       commands = parseOpCode(this.opCode());
//       con = this.compute(commands);
//     }

//     if (this.halt && this.next.halt) {
//       console.log(this.collectedOutputs);
//       return this.output;
//     }

//     this.next.setInput(this.output);
//     return this.next.run();

//   }

//   getValues(valParams) {
//     const { modes, pointer, rBase } = valParams;

//     const modeVals = [];

//     modes.reverse().forEach((mode, idx) => {
//       const curr = this.access(pointer + idx + 1);
//       let val;
//       if (mode === 2) {
//         val = this.access(rBase + curr);
//       } else if (mode === 1) {
//         val = curr;
//       } else {
//         val = this.access(curr);
//       }

//       modeVals[idx] = val;
//     })

//     return modeVals;
//   }

//   compute(commands) {

//     const { pointer, program } = this;
//     const [code, modes] = commands;
//     const valParams = { modes, pointer, rBase: this.relativeBase };
//     let x, y, z;

//     switch (code) {
//       case 99:
//         this.halt = true;
//         console.log('halting')
//         return false;
//       case 9:
//         [x] = this.getValues(valParams)

//         this.relativeBase = this.relativeBase + x;
//         this.updatePointer(pointer + 2);
//         console.log(`updated the relative base to ${this.relativeBase} and the pointer to ${this.pointer}`)
//         return true;
//       case 8:
//         [x, y, z] = this.getValues(valParams);
//         if (x === y) {
//           program[this.access(z)] = 1;
//           console.log(`updated idx ${this.access(z)} to 1`)
//         } else {
//           program[this.access(z)] = 0;
//           console.log(`updated idx ${this.access(z)} to 0`)
//         }

//         this.updatePointer(pointer + 4);
//         console.log(`updated pointer to ${this.pointer}`)
//         return true;
//       case 7:
//         [x, y, z] = this.getValues(valParams);
//         if (x < y) {
//           program[this.access(z)] = 1;
//           console.log(`updated idx ${this.access(z)} to 1`)
//         } else {
//           program[this.access(z)] = 0;
//           console.log(`updated idx ${this.access(z)} to 0`)
//         }

//         this.updatePointer(pointer + 4);
//         console.log(`updated pointer to ${this.pointer}`)
//         return true;
//       case 6:
//         [x, y] = this.getValues(valParams)
//         if (!x) {
//           this.updatePointer(y);

//           return true;
//         }

//         this.updatePointer(pointer + 3);
//         console.log(`updated pointer to ${this.pointer}`);
//         return true;
//       case 5:
//         [x, y] = this.getValues(valParams);
//         if (x) {
//           this.updatePointer(y);

//           return true;
//         }

//         this.updatePointer(pointer + 3);
//         console.log(`updated pointer to ${this.pointer}`);
//         return true;
//       case 4:
//         [x, y] = this.getValues(valParams);
//         this.updatePointer(pointer + 2);
//         this.setOutput(x);
//         this.collectedOutputs.push(this.output);
//         console.log(`updated output to ${this.output} and pointer to ${this.pointer}`);
//         return false;
//       case 3:
//         [x, y] = this.getValues(valParams);
//         if (this.setting) {
//           program[this.access(y)] = this.setting;
//           console.log(`used setting to update idx ${this.access(y)} to ${this.setting} `)
//           this.setting = null;
//         } else {
//           program[this.access(y)] = this.input;
//           console.log(`used input to update idx ${this.access(y)} to ${this.input} `)
//         }

//         this.updatePointer(pointer + 2);

//         return true;
//       case 2:
//         [x, y, z] = this.getValues(valParams);

//         program[this.access(z)] = x * y;

//         this.updatePointer(pointer + 4);
//         console.log(`updated idx ${this.access(z)} to ${x * y} and pointer to ${this.pointer}`)
//         return true;
//       case 1:
//         [x, y, z] = this.getValues(valParams);

//         program[this.access(z)] = x + y;

//         this.updatePointer(pointer + 4);
//         console.log(`updated idx ${this.access(z)} to ${x + y} and pointer to ${this.pointer}`)
//         return true;
//       default:
//         console.log(`Failed on sensor ${this.name}. Your code was ${code} and something went wrong`);
//         return false;
//     }
//   }
// }

// SensorNode.buildSingleNode = function (setting, program) {
//   const newNode = new SensorNode('0', setting, program);
//   newNode.next = newNode;
//   return newNode;
// }

// SensorNode.buildList = function (sequence, program) {
//   const nodeList = sequence.map((setting, idx) => {
//     return new SensorNode(idx, setting, program);
//   });

//   for (let i = 0; i < nodeList.length; i++) {
//     nodeList[i].setNext(nodeList[i + 1]);

//     if ((i + 1) === nodeList.length) nodeList[i].setNext(nodeList[0]);
//   }

//   return nodeList[0];
// };

// SensorNode.executeLoop = function (loop) {
//   return loop.run();
// };

// SensorNode.test = function(program) {
//   const testNode = this.buildSingleNode(1, program);
//   return testNode.run();
// }

// function parseOpCode(opCode) {
//   const parseable = opCode.toString();
//   const { length } = parseable;

//   // if (opCode === 4) return [opCode];
//   if (opCode === 3) return [opCode];

//   const command = parseInt(parseable.slice(length - 2));

//   // if (command === '99') return [0, 99]

//   let modes = parseable.slice(0, length - 2).split("").map(el => parseInt(el));
//   console.log(`pre modes = ${modes}`);
//   if (modes.length === 0) {
//     modes = [0, 0, 0];
//   } else {
//     switch(command) {
//       case 1:
//       case 2:
//       case 7:
//       case 8:
//         while (modes.length < 2) {
//           modes = [0].concat(modes)
//         }

//         break;
//       case 3:
//         if (modes.length < 2) {
//           if (modes[0] === 2) { modes = [2].concat(modes); break; }
//           modes = [0].concat(modes);
//         }
//         break;
//       default:
//         modes = [0, 0, 0];
//         break;
//     }
     
//   }

//   if (modes.length < 3) {

//     if (modes[0] === 2) {
//       modes = [2].concat(modes);
//     } else {
//       modes = [0].concat(modes);
//     }
//   }

//   console.log(`command = ${command}`)
//   console.log(`modes = ${modes}`)
//   return [command, modes];
// }

// // module.exports = SensorNode;

// console.log(SensorNode.test(getProgram()));
// // console.log(SensorNode.test([
// //   109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99]));
// // console.log(SensorNode.test([104, 1125899906842624, 99]))
// // console.log(SensorNode.test([1102, 34915192, 34915192, 7, 4, 7, 99, 0]))